const list = document.querySelector('.list');

const arr = ["Kharkiv", "Kyiv", "Borispol", "Irpin", "Odesa", "Lviv", "Dnipro"];

function createElem(userText) {
    let newElem = document.createElement('li');
    newElem.textContent = userText;
    newElem.className = "list";
    list.append(newElem);
}

createElem(arr);

let sec = 3;

function timeSec() {

if (sec < 1) {
    document.body.innerHTML = '';
}

else {
    document.getElementById('timer').innerHTML = sec;
    setTimeout(timeSec, 1000);
    sec--;
    }
}

timeSec();